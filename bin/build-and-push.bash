#!/bin/bash

TEMPORARY_BUG_FIX="--provenance=false"

CONTEXT=./src/
PLATFORMS="linux/amd64,linux/arm64"
REGISTRY=registry.gitlab.com/hfossedu/kits
NAME=kitclient
BRANCH="$(git branch --show-current)"
IMAGE="$REGISTRY/$NAME:$BRANCH"
BUILDER="$NAME-builder"

docker buildx create --name "$BUILDER" --use
docker buildx build \
    --pull --push \
    --platform "$PLATFORMS" \
    --tag "$IMAGE" \
    "$TEMPORARY_BUG_FIX" \
    "$CONTEXT"
docker buildx rm "$BUILDER"
