#!/usr/bin/env bash
SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd "${SCRIPT_DIR}/.."

# `docker build` configuration
DOCKERFILE="./src/Dockerfile"
BUILD_CONTEXT="./src"

# Name to use for local builds.
LOCAL_IMAGE_NAME="kitclient:dev"

if [[ -n "$PIPELINE_BUILDX_BUILD_OPTIONS" ]] ; then
    # Multi-platfrom build in pipeline.
    docker buildx build $PIPELINE_BUILDX_BUILD_OPTIONS \
        --file "$DOCKERFILE" "$BUILD_CONTEXT"
elif [[ -n "$PIPELINE_IMAGE_NAME" ]] ; then
    # Single-platform build in pipeline.
    docker build --tag $PIPELINE_IMAGE_NAME \
        --file "$DOCKERFILE" "$BUILD_CONTEXT"
else
    # Single-platform build in local.
    docker build --tag "$LOCAL_IMAGE_NAME" \
        --file "$DOCKERFILE" "$BUILD_CONTEXT"
fi
