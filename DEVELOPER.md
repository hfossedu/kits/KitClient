# Developer Documentation

Run all commands below in the same directory as this file.

## 2.1. Build image

```bash
bin/build.bash
```

## 2.2. Create container

```bash
bin/create.bash
```

## 2.3. Start container

```bash
bin/start.bash
```

This will print a couple of URLs you can use to interact with the container.

## 2.4. Stop the container

```bash
bin/stop.bash
```

## 2.5. Remove the stopped container

```bash
bin/rm.bash
```

## 2.6. Build, create, and start container

```bash
bin/up.bash
```

## 2.7. Stop and remove container

```bash
bin/stop.bash
```

## 2.8. Lint the code

```bash
bin/lint.bash
```

## 2.9. Build and push image

This is a maintainer operation.

You need to be authenticated with GitLab and have privileges to write to
this project's container registry.

Builds a multi-platform image and pushes it to the container registry
tagged with the current branch name.

```bash
bin/build-and-push.bash
```

## 2.10. Release image

This is a maintainer operation.

You need to be authenticated with GitLab and have privileges to write to
this project's container registry.

Tags the image tagged with `:main` in the container registry with version
tags and `:latest`. Pass the new version number as the first parameter.

For example, the following applies the tags `:0.1.0`, `:0.1`, `:0`,
and `:latest` to the image whose tag is `:main`.

```bash
bin/release.bash 0.1.0
```

---

## Notes

* This needs some polish but it works pretty well and is very responsive on my
  machine.
  * Make a nice user
  * etc...
* I started from an image that has TigerVNC installed
  * It runs both a VNC and a noVNC server.
  * It is being actively maintained with a new image within the last two weeks.
* One challenge is copy and paste between host and container
  * There is a clipboard hack in the noVNC client that works but is
    inconvenient.
  * The TigerVNC client works better, allows host/container copy/paste and is
    simple to install.
    * <https://tigervnc.org/>
    * Need to open another port to use the standalone client.
      * `docker run -p 6901:6901 -p 5901:5901 tiger`
    * The UI appears to be in french when I run the most recent version but I
      suspect this can be fixed.
* The need to use `--no-sandbox` flag on VSCode is a quick hack to fix the
  issue:
  * `Failed to move to new namespace: PID namespaces supported, Network
    namespace supported, but failed: errno = Operation not permitted`
  * Should be pretty easily fixable with some research.

Install

* Meld
* Gedit
* Vim
* Emacs
