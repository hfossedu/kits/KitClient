# KitClient

The KitClient is a Linux development environment that runs inside a Docker
container.

## Requirements

* [Docker](https://www.docker.com/)

## Running the KitClient

First, ensure Docker is installed and running.
Then start a Terminal/Command Prompt and enter the following:

<!-- markdownlint-disable MD013 -->
```bash
docker create --name KitClient --mount source=gitkitvol,target=/home/student -p 6901:6901 -p 5901:5901 registry.gitlab.com/hfossedu/kits/kitclient:latest
docker start KitClient
```
<!-- markdownlint-enable MD013 -->

The first line creates a Docker container named `KitClient`.  The
`\home\student` directory in the container is preserved in a Docker volume.
Thus, changes to the home directory will be preserved across stop/start and
delete/create cycles of the container.
Any software installed in the container (e.g. `apt-get install`) will be
preserved in the container's writeable layer.  Thus, these changes will be
preserved across stop/start cycles of the container, but not across
delete/create cycles.  The second line starts the container running so that
you can connect to it using a VNC Viewer.

## Run a specific version of KitClient

Append the version number to the end of the KitClient's image tag when
you create it. For example, if you want to run version 1.2.3, append
`:1.2.3` to the image tag in the `create` command as follows.

<!-- markdownlint-disable MD013 -->
```bash
docker create --name KitClient --mount source=gitkitvol,target=/home/student -p 6901:6901 -p 5901:5901 registry.gitlab.com/hfossedu/kits/kitclient:1.2.3 
```
<!-- markdownlint-enable MD013 -->

## Connect to the KitClient through a VNC client

This method of interacting with a KitClient provides the best experience,
but requires that you install a VNC client. We suggest installing
[Tiger VNC](https://tigervnc.org/).

Once Tiger VNC is installed, start it, and open `localhost:5901`.

## Connect to the KitClient through a Web browser

This method of interacting with a KitClient is easier to get started,
but it's interface is not as nice as a VNC client.

Point a browser to `http://localhost:6901`.

## Stop the KitClient

Assuming you started the KitClient as describe above, you may use the
following command to stop the KitClient, and your files will be safely
stored in the `kitclient` directory.

```bash
docker stop KitClient
```

## Restart the KitClient

If the KitClient container exists, it can be restarted by using the command:

```bash
docker start KitClient
```

If the container has been deleted, refer to the Running The KitClient above
to create the container and then start it.

## Delete the KitClient's state

Assuming you ran the KitClient as described above, the full state of the
KitClient can be removed using the commands:

```bash
docker rm KitClient
docker volume rm gitkitvol
```

Removing the KitClient container deletes any state saved in the container's
writeable layer.  Deleting the gitkitvolume deletes any changes to the student
user's home directory.

## Uninstalling KitClient

Follow the above steps to delete the KitClient's state.  Then use the command:

```bash
docker image rm registry.gitlab.com/hfossedu/kits/kitclient:latest
```

## Development

### Testing Locally

Build a local image for testing:

```bash
bin/build.bash
bin/create.bash
bin/start.bash
```

or

```bash
bin/up.bash
```

### Build and Push Multi-architecture image for Testing

<!-- markdownlint-disable MD013 -->
```bash
bin/bind-and-push.bash

docker create --name KitClient -p 6901:6901 -p 5901:5901 registry.gitlab.com/hfossedu/kits/kitclient:<branch name>

docker start KitClient
```
<!-- markdownlint-enable MD013 -->

### Making a Release

```bash
bin/release.bash <sem ver number>
```
